package app.com.talogger.fileWrite;

public interface PermissionListener {
    void onAccepted();
    void onDenied();
}
