package app.com.demologapp;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import app.com.talogger.TALog;


public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
//        TALog.debug("This is a debug test");

    }
    public void taLog(View v){

        TALog.debug("This is a debug test");
    }
}
