package app.com.demologapp;

import android.app.Application;

import app.com.talogger.LogLevel;
import app.com.talogger.TALog;

public class DemoLogApp extends Application {


    @Override
    public void onCreate() {
        super.onCreate();
        TALog.init(this,BuildConfig.DEBUG ? LogLevel.ALL : LogLevel.NONE);

        TALog.enableBorder(true);
        TALog.enableThreadInfo(true);
        TALog.enableLineTracing(true);
        TALog.isPermissionAllow(false);
    }

}
